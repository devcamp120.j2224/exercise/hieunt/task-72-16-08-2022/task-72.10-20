package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@CrossOrigin
	@GetMapping("/regions")
	public List<CRegion> getAllRegions() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions")
    public List<CRegion> getRegionsByCountryId(@PathVariable Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/regions/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}

	@CrossOrigin
	@PostMapping("/countries/{countryId}/regions")
	public ResponseEntity<Object> createRegion(@PathVariable("countryId") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
				CRegion newRole = new CRegion();
				newRole.setRegionName(cRegion.getRegionName());
				newRole.setRegionCode(cRegion.getRegionCode());				
				CCountry _country = countryData.get();
				newRole.setCountry(_country);
				newRole.setCountryName(_country.getCountryName());
				return new ResponseEntity<>(regionRepository.save(newRole), HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
				.body("Failed to Create specified Region: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/regions/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CRegion> regionData = regionRepository.findById(id);
			if (regionData.isPresent()) {
				CRegion newRegion = regionData.get();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());
				return new ResponseEntity<>(regionRepository.save(newRegion), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity()
				.body("Failed to Update specified Region: "+e.getCause().getCause().getMessage());
		}
	}

	@CrossOrigin
	@DeleteMapping("/regions/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e.getCause().getCause().getMessage());
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/regions")
    public List<CRegion> countRegionByCountryCode(@RequestParam String countryCode) {
        return regionRepository.findByCountryCountryCode(countryCode);
    }

	@CrossOrigin
	@GetMapping("/country/{countryId}/region/{regionId}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable Long countryId,@PathVariable Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId, countryId);
    }

	@CrossOrigin
	@GetMapping("/countries/{countryId}/regions/count")
    public int countRegionByCountryId(@PathVariable Long countryId) {
        return regionRepository.findByCountryId(countryId).size();
    }
	
	@CrossOrigin
	@GetMapping("/regions/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}		
}

